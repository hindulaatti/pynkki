----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 11/06/2019 02:04:35 PM
-- Design Name: 
-- Module Name: lab45_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity lab45_tb is
--  Port ( );
end lab45_tb;

architecture Behavioral of lab45_tb is
    component lab45 is
        Generic (
            BASE_FREQ : integer := 125e6;
            OUTPUT_FREQ : integer := 250e3
        );
        Port ( 
            Reset: in std_logic;
            btn: in std_logic;
            sysclk: in std_logic;
            led5_r: out std_logic;
            led5_g: out std_logic;
            led5_b: out std_logic
        );
    end component lab45;
    
    -- Clock period definitions
    constant SYSCLK_PERIOD : time := 8 ns; -- 125MHz
    
    -- Inputs
    signal sysclk: STD_LOGIC := '0'; -- init to 0 for simulation
    signal Reset : STD_LOGIC := '0'; -- init to 0 for simulation
    signal btn : STD_LOGIC := '0';
    
    --Outputs
    signal led5_r: STD_LOGIC := '0';
    signal led5_g: STD_LOGIC := '0';
    signal led5_b: STD_LOGIC := '0';    
    
    
begin
    
    -- Instantiate the Device Under Test (DUT) 
    i_DUT: lab45
    port map (
        sysclk => sysclk,
        Reset => Reset,
        btn => btn,
        led5_r => led5_r,
        led5_g => led5_g,
        led5_b => led5_b
    );
    
    clk_process :process
        begin
            sysclk <= '0';
            wait for SYSCLK_PERIOD;
            sysclk <= '1';
            wait for SYSCLK_PERIOD;
        end process;

    -- Inputs
    -- Here we create the stimulus (select signal)
    -- The data inputs in this case are constant

    -- Stimulus process
    stim_proc: process
    begin  
        -- hold reset state for 100 ns.
        wait for 100 ns;
        Reset <= '1';
        wait for 100 ns;
        Reset <= '0';
        
        wait for SYSCLK_PERIOD;
        btn <= '0';
        wait for 1000 ns; 
        btn <= '1';
        wait for 10000 ns; 
        btn <= '0';
        wait for 2000 ns; 
        btn <= '1';
        wait for 10000 ns; 
    
    end process;

end Behavioral;
