----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 10/23/2019 09:44:41 AM
-- Design Name: 
-- Module Name: clock_divider - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity clock_divider is
    Generic(
        BASE_FREQ : integer := 125e6;
        OUTPUT_FREQ : integer := 250e3
    );
    Port ( 
        n_Reset : in std_logic;
        sysclk : in std_logic;
        clk_out : out std_logic
    );
end clock_divider;

architecture rtl of clock_divider is
    constant CLOCK_OUT_TICKS: integer := (BASE_FREQ/OUTPUT_FREQ);
    constant HALF_CLOCK_OUT_TICKS: integer := (BASE_FREQ/OUTPUT_FREQ)/2;
    
    signal tick: integer range 0 to CLOCK_OUT_TICKS;

begin
    clock_p: process (sysclk, n_Reset)
    begin
        if(n_Reset='0') then
            tick <= 0;
            clk_out <= '0';
        elsif(rising_edge(sysclk)) then
            tick <= tick + 1;
        end if;
        
        if(tick >= CLOCK_OUT_TICKS) then
            tick <= 0;
            clk_out <= '0';
        elsif(tick >= HALF_CLOCK_OUT_TICKS) then
            clk_out <= '1';
        end if;
    end process clock_p;

end rtl;
