----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 21.10.2019 19:08:27
-- Design Name: 
-- Module Name: button_logic - rtl
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity button_logic is
  generic ( delay: integer := 1000;         -- delay before pulse train generation starts 
            interval: integer := 500        -- interval between individual pulses
        );
  port ( clock_in : in STD_LOGIC;           -- input clock
         n_Reset : in STD_LOGIC;
         btn : in STD_LOGIC;                -- input signal to be debounced
         output : out STD_LOGIC             --debounced signal
         );
end button_logic;

architecture rtl of button_logic is
    
    type pulser_state_t is (Idle, Armed, Repeat);
    signal pulser_state: pulser_state_t;
    
    --signal
    signal interval_counter : integer range 0 to interval := 0;
    signal delay_counter :  integer range 0 to delay := 0;
    signal outArmed: std_logic := '0';
    signal outRepeat: std_logic := '0';

begin

    button_pulser: process (clock_in, n_Reset) begin
        if (n_Reset = '0') then
            pulser_state <= Idle;
        elsif (clock_in'event and clock_in = '1') then
            case pulser_state is
                when Idle =>
                    if (btn = '1') then
                        outArmed <= '1'; -- generate a single shot
                        pulser_state <= Armed;
                    end if;
                when Armed => -- waiting for long press to happen     
                    if (btn = '1') then
                        outArmed <= '0';
                        if (delay_counter < delay) then
                            delay_counter <= delay_counter + 1;
                        else
                            pulser_state <= Repeat;
                            delay_counter <= 0;   
                        end if;
                    else 
                        pulser_state <= Idle;
                        outArmed <= '0';
                        delay_counter <= 0;
                    end if;
                when Repeat => -- button pressed, generating repeated pulses
                    if (btn = '1') then                                             
                        if (interval_counter < interval) then       -- when counter
                            interval_counter <= interval_counter + 1;   -- counter counts 
                            outRepeat <= '0';                                                    
                        else
                            outRepeat <= '1';
                            interval_counter <= 0;  
                        end if;
                    else -- exit when the button is release
                        pulser_state <= Idle;
                        outRepeat <= '0'; 
                        interval_counter <= 0;     
                    end if;
                when others =>
                    pulser_state <= Idle; -- just in case
            end case;
        end if; --clk/rst
    end process button_pulser;

output <= outArmed OR outRepeat;

end rtl;

