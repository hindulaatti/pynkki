----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 22.10.2019 14:30:02
-- Design Name: 
-- Module Name: lab_4_top - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity lab_4_top is
    Generic (
        ClockFrequencyHz: integer := 1000
    );
    Port ( 
        sysclk : in STD_LOGIC;
        Reset : in STD_LOGIC;
        btn : in STD_LOGIC;
        led5_r : out STD_LOGIC;
        led5_g : out STD_LOGIC;
        led5_b : out STD_LOGIC
    );
end lab_4_top;

architecture Behavioral of lab_4_top is

component gen_clock_divider is
    generic (
        output_freq: integer := 1000   -- 1kHz clock signal
    );
    port ( 
        sysclk : in STD_LOGIC;
        n_Reset : in STD_LOGIC;
        clock_out : out STD_LOGIC
    );
end component gen_clock_divider;

component button_logic is
    generic ( 
        delay: integer := 10000;       -- delay before pulse train generation starts 
        interval: integer := 5000   -- interval between individual pulses
    );
    port ( 
        clock_in : in STD_LOGIC;         -- input clock
        n_Reset : in STD_LOGIC;
        btn : in STD_LOGIC;            -- input signal to be debounced
        output : out STD_LOGIC         --debounced signal
    );
end component;

-- Enumerated type declaration and state signal declaration
type RGB_State is (Red, Green, Blue);
signal State : RGB_State;

signal clock: STD_LOGIC;
signal RGB_led5: STD_LOGIC_VECTOR(0 to 2);
signal button_out: STD_LOGIC;
signal n_Reset: STD_LOGIC;

begin
     
    i_clock_1kHz : gen_clock_divider
    generic map (
        Output_freq => ClockFrequencyHz
    )
    port map ( 
        sysclk => sysclk,
        n_Reset => n_Reset,
        clock_out => clock
    );
    
    i_button_logic : button_logic
    generic map (
        delay => 2000,      -- delay before pulse train generation starts 
        interval => 500   -- interval between individual pulses
    )
    port map (
        clock_in => clock,         -- input clock
        n_Reset => n_Reset,
        btn => btn,            -- input signal to be debounced
        output => button_out   -- debounced signal
    );
    
   -- Map signal to actual output ports
    n_Reset <= NOT Reset;
    
    led5_r <= RGB_led5(2);
    led5_g <= RGB_led5(1);
    led5_b <= RGB_led5(0);
    
    
    rgb5_ctrl_p: process(clock, n_Reset)
    begin
        if n_Reset = '0' then
            State <= Red;
        elsif (clock'event and clock = '1') then
            case State is
                when Red =>
                    RGB_led5 <=  "001"; --red
                    if (button_out = '1') then
                        State <= Green;
                    end if;
                when Green =>
                    RGB_led5 <=  "010"; --green
                    if (button_out = '1') then      
                        State <= Blue;
                    end if;
                when Blue =>
                    RGB_led5 <=  "100"; --blue
                    if (button_out = '1') then
                        State <= Red;
                    end if;
            end case;  
        end if; --clk/rst                      
    end process rgb5_ctrl_p; 
    

end Behavioral;
