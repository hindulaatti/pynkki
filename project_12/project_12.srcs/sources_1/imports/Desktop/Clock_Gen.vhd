----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 21.10.2019 19:24:15
-- Design Name: 
-- Module Name: gen_clock_divider - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity gen_clock_divider is
    generic (
		output_freq: integer := 1000
	);
    port ( sysclk : in STD_LOGIC;
           n_Reset : in STD_LOGIC;
           clock_out : out STD_LOGIC);
end gen_clock_divider;

architecture Behavioral of gen_clock_divider is
 
    constant CLOCK_OUT_TICKS : integer := (125e6/output_freq);
    constant HALF_CLOCK_OUT_TICKS : integer := (125e6/output_freq)/2;
    
    signal counter : integer range 0 to CLOCK_OUT_TICKS := 0;
    
begin

    frequency_divider: process (n_Reset, sysclk) 
    begin
        if (n_Reset = '0') then
            clock_out <= '0';
            counter <= 0;
        elsif (sysclk'event and sysclk = '1') then
            if (counter <= CLOCK_OUT_TICKS) then
                counter <= counter + 1;
            else  
                counter <= 0;
            end if;
            if (counter <= HALF_CLOCK_OUT_TICKS) then
                clock_out <= '0';
            else
                clock_out <= '1';
            end if;
        end if;
    end process;

end Behavioral;
