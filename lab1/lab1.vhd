----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 09/11/2019 08:30:47 AM
-- Design Name: 
-- Module Name: lab1 - rtl
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity lab1 is
    Port ( btn : in STD_LOGIC_VECTOR (3 downto 0);
           sw : in STD_LOGIC_VECTOR (1 downto 0);
           led : out STD_LOGIC_VECTOR (3 downto 0);
           led4_r : out std_logic;
           led4_g : out std_logic;
           led4_b : out std_logic;
           led5_r : out std_logic;
           led5_g : out std_logic;
           led5_b : out std_logic
           );
end lab1;

architecture rtl of lab1 is

    -- group of RGB led signals
    signal RGB_Led_4: std_logic_vector(0 to 2); -- order R, G, B
	signal RGB_Led_5: std_logic_vector(0 to 2); -- order R, G, B
	signal RGB_Led_mem: std_logic_vector(0 to 2);
	
begin
    -- map signal "RGB_Led_5" to actual output ports
    led5_r <= RGB_Led_5(2);
    led5_g <= RGB_Led_5(1);
    led5_b <= RGB_Led_5(0);

    -- Some "housekeeping" first
    -- map signal "RGB_Led_4" to actual output ports
    led4_r <= RGB_Led_4(2);
    led4_g <= RGB_Led_4(1);
    led4_b <= RGB_Led_4(0);
    
    led_ctrl: process(btn(1), btn(0), sw(1))
    begin
       if sw(1) = '0' then
           case btn(1 downto 0) is
               when "00" =>
                   led <= "0001";
               when "01" =>
                   led <= "0010";
               when "10" =>
                   led <= "0100";
               when "11" =>
                   led <= "1000";
            end case;
        else
           led(0) <= btn(0) and btn(1);
           led(1) <= btn(0) xor btn(1);
           led(2) <= btn(0) or btn(1);
           led(3) <= btn(0) nand btn(1);
       end if;
    end process led_ctrl;
    
    -- asdfasdf
    
    -- Control of RGB LEDS
    with btn(1 downto 0) select
        RGB_Led_4 <=    "001" when "01",
                        "010" when "10",
                        "100" when "11",
                        "000" when others;
                        
    RGB_Led_mem <= RGB_Led_4 when sw(0)='1' else "000";
    RGB_Led_5 <= "111" when sw(1)='1' else RGB_Led_mem;


end rtl;
