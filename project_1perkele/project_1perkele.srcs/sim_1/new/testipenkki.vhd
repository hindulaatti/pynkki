----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 11/13/2019 11:44:20 AM
-- Design Name: 
-- Module Name: testipenkki - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity testipenkki is
--  Port ( );
end testipenkki;

architecture Behavioral of testipenkki is

    constant SYSCLK_PERIOD : time := 8 ns; -- 125MHz
    signal sysclk: std_logic := '0'; -- init to 0 for simulation
    signal n_Reset : std_logic := '0'; -- init to 0 for simulation
    signal btn : std_logic_vector(1 downto 0) := (others => '0');
    signal output : std_logic := '0';
    signal button_output : std_logic_vector(1 downto 0) := (others => '0');
    
    
    component pwm is
        Generic(
            BASE_freq : integer := 125e6;
            PWM_resolution : integer := 10;
            PWM_freq : integer := 10e3
        );
        Port ( 
            n_Reset : in std_logic;
            sysclk : in std_logic;
            btn : in std_logic_vector(1 downto 0) := (others => '0');
            output : out std_logic
        );
    end component pwm;
            
    component button_logic is
        Generic(
            repeat_delay : integer := 1000;
            repeat_rate : integer := 500
        );
        Port ( 
            n_Reset : in std_logic;
            btn : in std_logic;
            clock_in : in std_logic;
            output : out std_logic
        );
    end component;

begin
    -- Clock driver
    sysclk <= not sysclk after (SYSCLK_PERIOD / 2.0);
    
    i_pwm : pwm
    generic map (
        BASE_freq => 125e6,
        PWM_resolution => 10,
        PWM_freq => 10e3
    )
    port map (
        n_Reset => n_Reset,
        sysclk => sysclk,
        btn => button_output,
        output => output
    );
    
    i_button_logic_0 : button_logic
    generic map (
        repeat_delay => 10000,
        repeat_rate => 5000
    )
    port map ( 
        n_Reset => n_Reset,
        btn => btn(0),
        clock_in => sysclk,
        output => button_output(0)
    );
    
    i_button_logic_1 : button_logic
    generic map (
        repeat_delay => 10000,
        repeat_rate => 5000
    )
    port map ( 
        n_Reset => n_Reset,
        btn => btn(1),
        clock_in => sysclk,
        output => button_output(1)
    );

    -- Here we create the stimulus (select signal)
    -- The data inputs in this case are constant
    stimulus_p: process
    begin
        -- after 10 clock cycles deassert reset
        wait for ( SYSCLK_PERIOD * 10 );
        n_Reset <= '1';
        
        wait for 500 us; -- this is just unconditional wait for a specified amount of time
        btn <= "01";
        
        wait for 500 us; -- this is just unconditional wait for a specified amount of time
        btn <= "10";
                
        wait for 500 us; -- this is just unconditional wait for a specified amount of time
        btn <= "00";
        
        wait for 500 us; -- this is just unconditional wait for a specified amount of time
        
        wait; -- wait here forever
    
    end process;

end Behavioral;
