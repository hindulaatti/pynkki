----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 10/23/2019 09:44:41 AM
-- Design Name: 
-- Module Name: clock_divider - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity button_logic is
    Generic(
        repeat_delay : integer := 1000;
        repeat_rate : integer := 500
    );
    Port ( 
        n_Reset : in std_logic;
        btn : in std_logic;
        clock_in : in std_logic;
        output : out std_logic
    );
end button_logic;

architecture Behavioral of button_logic is
    
    type pulser_state_t is (Idle, Armed, Repeat);
    signal pulser_state: pulser_state_t;
    
    signal delay_counter: integer range 0 to repeat_delay := 0;
    signal repeat_counter: integer range 0 to repeat_rate := 0;
    

begin
    button_logic_p: process (clock_in, n_Reset)
    begin
        if(n_Reset='0') then
            delay_counter <= 0;
            repeat_counter <= 0;
            output <= '0';
            pulser_state <= Idle;
        elsif(rising_edge(clock_in)) then
            case pulser_state is
                when Idle =>
                    if (btn = '1') then
                        pulser_state <= Armed;
                    end if;
                when Armed =>
                    output <= '0';
                    if (btn = '0') then
                        pulser_state <= Idle;
                        delay_counter <= 0;
                    else
                        if (delay_counter = 0) then
                            output <= '1';
                        end if;
                        
                        if (delay_counter >= repeat_delay - 1) then
                            delay_counter <= 0;
                            pulser_state <= Repeat;
                        else
                            delay_counter <= delay_counter + 1;
                        end if;
                    end if;
                when Repeat =>
                    output <= '0';
                    if (btn = '0') then
                        pulser_state <= Idle;
                        repeat_counter <= 0;
                    else
                        if (repeat_counter = 0) then
                            output <= '1';
                        end if;
                        
                        if (repeat_counter >= repeat_delay - 1) then
                            repeat_counter <= 0;
                        else
                            repeat_counter <= repeat_counter + 1;
                        end if;
                    end if;
            end case;
        end if;
    end process button_logic_p;

end Behavioral;
