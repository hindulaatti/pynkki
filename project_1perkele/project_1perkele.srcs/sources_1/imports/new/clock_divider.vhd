----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 10/23/2019 09:44:41 AM
-- Design Name: 
-- Module Name: clock_divider - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity clock_divider is
    Generic(
        BASE_FREQ : integer := 125e6;
        OUTPUT_FREQ : integer := 1000
    );
    Port ( 
        n_Reset : in std_logic;
        sysclk : in std_logic;
        clock_out : out std_logic
    );
end clock_divider;

architecture Behavioral of clock_divider is
    constant CLOCK_OUT_TICKS: integer := (BASE_FREQ/OUTPUT_FREQ);
    constant HALF_CLOCK_OUT_TICKS: integer := (BASE_FREQ/OUTPUT_FREQ)/2;
    
    signal clk_counter: integer range 0 to CLOCK_OUT_TICKS := 0;

begin
    clock_p: process (sysclk, n_Reset)
    begin
        if (n_Reset = '0') then
            clock_out <= '0';
            clk_counter <= 0;
        elsif (sysclk'event and sysclk = '1') then
            if (clk_counter <= CLOCK_OUT_TICKS) then
                clk_counter <= clk_counter + 1;
            else  
                clk_counter <= 0;
            end if;
            if (clk_counter <= HALF_CLOCK_OUT_TICKS) then
                clock_out <= '0';
            else
                clock_out <= '1';
            end if;
        end if;
    end process clock_p;

end Behavioral;
