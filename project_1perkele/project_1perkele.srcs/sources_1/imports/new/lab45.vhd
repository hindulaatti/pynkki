----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 11/06/2019 12:44:49 PM
-- Design Name: 
-- Module Name: lab45 - rtl
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity lab45 is
    Generic (
        BASE_FREQ : integer := 125e6;
        OUTPUT_FREQ : integer := 1000
    );
    Port ( 
        Reset: in std_logic;
        btn: in std_logic_vector(2 downto 0);
        sysclk: in std_logic;
        led5_r: out std_logic;
        led5_g: out std_logic;
        led5_b: out std_logic
    );
end lab45;

architecture Behavioral of lab45 is
    
    signal clock: std_logic;
    signal RGB_led5: std_logic_vector(2 downto 0);
    signal pwm_output: std_logic;
    signal pwm_output_vector: std_logic_vector(2 downto 0);
    signal button_out: std_logic_vector(2 downto 0);
    signal n_Reset: std_logic;

    component clock_divider is
        Generic(
            BASE_FREQ : integer := 125e6;
            OUTPUT_FREQ : integer := 1000
        );
        Port ( 
            n_Reset : in std_logic;
            sysclk : in std_logic;
            clock_out : out std_logic
        );
    end component;
    
    component button_logic is
        Generic(
            repeat_delay : integer := 1000;
            repeat_rate : integer := 500
        );
        Port ( 
            n_Reset : in std_logic;
            btn : in std_logic;
            clock_in : in std_logic;
            output : out std_logic
        );
    end component;
    
    component pwm is
        Generic(
            BASE_freq : integer := 125e6;
            PWM_resolution : integer := 10;
            PWM_freq : integer := 10e3
        );
        Port ( 
            n_Reset : in std_logic;
            sysclk : in std_logic;
            btn : in std_logic_vector(1 downto 0) := (others => '0');
            output : out std_logic
        );
    end component pwm;
    
begin
    n_Reset <= NOT Reset;
    led5_r <= RGB_led5(2);
    led5_g <= RGB_led5(1);
    led5_b <= RGB_led5(0);
    pwm_output_vector <= (others => pwm_output);
    
    i_clock : clock_divider
    generic map (
        BASE_FREQ => 125e6,
        OUTPUT_FREQ => 1000
    )
    port map (
        n_Reset => n_Reset,
        sysclk => sysclk,
        clock_out => clock
    );
    
    i_button_logic : button_logic
    generic map (
        repeat_delay => 1000,
        repeat_rate => 500
    )
    port map ( 
        n_Reset => n_Reset,
        btn => btn(0),
        clock_in => clock,
        output => button_out(0)
    );
            
    i_pwm : pwm
    generic map (
        BASE_freq => 125e6,
        PWM_resolution => 10,
        PWM_freq => 10e3
    )
    port map (
        n_Reset => n_Reset,
        sysclk => sysclk,
        btn => button_out,
        output => pwm_output
    );
    
    i_button_logic_0 : button_logic
    generic map (
        repeat_delay => 10000,
        repeat_rate => 5000
    )
    port map ( 
        n_Reset => n_Reset,
        btn => btn(1),
        clock_in => sysclk,
        output => button_out(1)
    );
    
    i_button_logic_1 : button_logic
    generic map (
        repeat_delay => 10000,
        repeat_rate => 5000
    )
    port map ( 
        n_Reset => n_Reset,
        btn => btn(2),
        clock_in => sysclk,
        output => button_out(2)
    );

    rgb5_ctrl_p: process(button_out, n_Reset)
    begin
        if n_Reset = '0' then
            RGB_led5 <= "001";
        elsif (button_out(0)'event and button_out(0) = '1') then
            case RGB_led5 is
                when "001" =>
                    RGB_led5 <= "010" AND pwm_output_vector;
                when "010" =>
                    RGB_led5 <= "100" AND pwm_output_vector;
                when others =>
                    RGB_led5 <= "001" AND pwm_output_vector;
             end case;
        end if;
    end process;
end Behavioral;
