----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 11/13/2019 11:26:55 AM
-- Design Name: 
-- Module Name: pwm - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity pwm is
    Generic(
        BASE_freq : integer := 125e6;
        PWM_resolution : integer := 10;
        PWM_freq : integer := 10e3
    );
    Port ( 
        n_Reset : in std_logic;
        sysclk : in std_logic;
        btn : in std_logic_vector(1 downto 0) := (others => '0');
        output : out std_logic
    );
end pwm;

architecture Behavioral of pwm is
    
    constant CLOCK_OUT_TICKS: integer := (BASE_freq/PWM_freq);
    constant RESOLUTION_MULTIPLIER: integer := (CLOCK_OUT_TICKS / PWM_resolution);
    signal clk_counter: integer range 0 to CLOCK_OUT_TICKS := 0;
    signal PWM_step: integer range 0 to PWM_resolution := 3;

begin
    
    button_p: process (btn, n_Reset)
    begin
        if (n_Reset = '0') then
            PWM_step <= 0;
        elsif ((btn(0)'event or btn(1)'event) and btn = "01") then
            if (PWM_step + 1 >= PWM_resolution) then
                PWM_step <= PWM_resolution;
            else 
                PWM_step <= PWM_step + 1;
            end if;
           
        elsif (btn'event and btn = "10") then
            if (PWM_step - 1 <= 0) then
                PWM_step <= 0;
            else 
                PWM_step <= PWM_step - 1;
            end if;
        end if;
    end process button_p;
    
    pwm_p: process (sysclk, n_Reset)
    begin
        if (n_Reset = '0') then
            output <= '0';
            clk_counter <= 0;
        elsif (sysclk'event and sysclk = '1') then
            if (clk_counter < PWM_step * RESOLUTION_MULTIPLIER) then
                output <= '1';
            else
                output <= '0';
            end if;
            
            if (clk_counter < CLOCK_OUT_TICKS) then
                clk_counter <= clk_counter + 1;
            else  
                clk_counter <= 0;
            end if;
        end if;
    end process pwm_p;

end Behavioral;
